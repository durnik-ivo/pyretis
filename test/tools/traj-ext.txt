# Cycle: 0, status: ACC
#     Step              Filename       index    vel
         0             trajB.trr           6     -1
         1             trajB.trr           5     -1
         2             trajB.trr           4     -1
         3             trajB.trr           3     -1
         4             trajB.trr           2     -1
         5             trajB.trr           1     -1
         6             trajB.trr           0     -1
         7             trajF.trr           0      1
         8             trajF.trr           1      1
         9             trajF.trr           2      1
        10             trajF.trr           3      1
        11             trajF.trr           4      1
        12             trajF.trr           5      1
        13             trajF.trr           6      1
        14             trajF.trr           7      1
